# WikiEducator Repository for Moodle #

This is a Moodle 2.0 repository that allows access to WikiEducator resources. It is derived from the wikimedia repository by Dongsheng Cai and Raul Kern that is included in the Moodle 2.0 sources.

Jim Tittsler, 2010